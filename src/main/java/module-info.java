module cz.cvut.fit.kosapi2timetabler.kosapi2timetabler_db.cli {
    requires cz.cvut.fit.kosapi2timetabler.exporter_to_db;
    requires cz.cvut.fit.kosapi2timetabler.kosapi_data_provider;
    requires java.xml.bind;
    requires org.hibernate.orm.core;
    requires java.sql;
}
