/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.kosapi2timetabler.kosapi2timetabler_db.cli;

import cz.cvut.fit.kosapi2timetabler.exporter_to_db.ExportKosSaveToDb;
import cz.cvut.fit.kosapi2timetabler.kosapi_data_provider.KosapiSettings;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.*;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

/**
 *
 * @author guthondr
 */
public class Kosapi2TimetablerDbCli {
    private static final Properties settings = new Properties();
    private static final String PROPERTY_SEMESTER_CODE = "cz.cvut.fit.kosapi2timetable.semester_code";
    private static final String PROPERTY_START_DATE = "cz.cvut.fit.kosapi2timetable.semester_start_date";
    private static final String PROPERTY_WEEKS_BOTH_MASK = "cz.cvut.fit.kosapi2timetable.weeks_both_mask";
    private static final String PROPERTY_WEEKS_EVEN_MASK = "cz.cvut.fit.kosapi2timetable.weeks_even_mask";
    private static final String PROPERTY_WEEKS_ODD_MASK = "cz.cvut.fit.kosapi2timetable.weeks_odd_mask";

    private static final String SETTINGS_FILENAME_DEFAULT = ".kosapi2db.properties";
    private static final Path SETTINGS_FILE = Paths.get(
            System.getProperty("cz.cvut.fit.kosapi2timetable.timetable_aggregator.settings_file", Paths.get(System.getProperty("user.home"), SETTINGS_FILENAME_DEFAULT).toString()));

    public static void main(String[] args) throws SQLException {
        EntityManagerFactory ENTITY_MANAGER_FACTORY = null;
        EntityManager ENTITY_MANAGER = null;

        KosapiSettings.getInstance().setFileStoreLoadSettings(SETTINGS_FILE);


        try {
            try {
                settings.load(Files.newBufferedReader(SETTINGS_FILE));
                ENTITY_MANAGER_FACTORY = Persistence.createEntityManagerFactory("TimetablerPG", settings);
                ENTITY_MANAGER = ENTITY_MANAGER_FACTORY.createEntityManager();
            } catch (final IOException e) {
                System.err.println(e);
            }
            if (!settings.containsKey(PROPERTY_SEMESTER_CODE) ||
                    !settings.containsKey(PROPERTY_WEEKS_BOTH_MASK) ||
                    !settings.containsKey(PROPERTY_WEEKS_EVEN_MASK) ||
                    !settings.containsKey(PROPERTY_WEEKS_ODD_MASK))
                throw new RuntimeException("These properties must be set in " + SETTINGS_FILE + ": "
                        + PROPERTY_SEMESTER_CODE + ", "
                        + PROPERTY_START_DATE + ", "
                        + PROPERTY_WEEKS_BOTH_MASK  + ", "
                        + PROPERTY_WEEKS_EVEN_MASK + ", "
                        + PROPERTY_WEEKS_ODD_MASK);
            final LocalDate START_DATE = LocalDate.parse(settings.getProperty(PROPERTY_START_DATE));

            final ExportKosSaveToDb saver = new ExportKosSaveToDb(
                    settings.getProperty(PROPERTY_WEEKS_BOTH_MASK),
                    settings.getProperty(PROPERTY_WEEKS_EVEN_MASK),
                    settings.getProperty(PROPERTY_WEEKS_ODD_MASK),
                    ENTITY_MANAGER,
                    settings.getProperty(PROPERTY_SEMESTER_CODE));

            final EntityTransaction et = ENTITY_MANAGER.getTransaction();

            et.begin();
            saver.saveFullExportToDb();
            et.commit();

        } finally {
            ENTITY_MANAGER.close();
            ENTITY_MANAGER_FACTORY.close();
        }
    }

}
